#!/usr/bin/env bash
# Script to create a docker image, register a new batch job definition with that docker image, and create a DAG
# in the passed managed airflow environment

# Parse the command line arguments
NAME=$1-$2
BRANCH=$2
VERSION=$3
REPO=$4
S3_LOCATION=$5
DAG=$6
CONTAINER_VPCUS=$7
CONTAINER_MEMORY_MB=$8

echo "Creating $NAME from branch $BRANCH version $VERSION..."
echo "Will publish image to rep $REPO,"
echo "and create AWS Batch job definition for $CONTAINER_VPCUS vcpus and $CONTAINER_MEMORY_MB Mb RAM,"
echo "and upload $DAG as $NAME.py to $S3_LOCATION..."

# set up the environment
pip3 install awscli

# Log docker in to the the ECR repo in us-east-1
eval $(aws ecr get-login --no-include-email --region us-east-1 | sed 's;https://;;g')

# Create the docker image and publish it to the relevant repository in the registry
IMAGE="$REPO:$NAME"
docker image build --compress -t $IMAGE -t $IMAGE-v$VERSION .
docker image ls
docker image push --help
docker image push $IMAGE
docker image push $IMAGE-v$VERSION

# Set up the memory in Mb and CPU required for this job
CONTAINER_PROPERTIES='{"image":"'$IMAGE'", "vcpus":'$CONTAINER_VPCUS', "memory":'$CONTAINER_MEMORY_MB'}'

# Register the batch job definition to run the image
aws batch register-job-definition --container-properties "$CONTAINER_PROPERTIES" --job-definition-name $NAME --type container --region us-east-1

# Create a DAG file for the relevant branch and upload to the DAGs folder
sed 's/{branch}/'$BRANCH'/' $DAG >> $NAME.py
aws s3 cp $NAME.py $S3_LOCATION

# Summarize
echo "Complete!"